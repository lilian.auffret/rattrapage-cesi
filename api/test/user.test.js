const expect = require('chai').expect;

var User = require('../models/user.model');

describe('User model test', () => {
    it('should be invalid if firstname is not required', (done) => {
        let u = new User();
        
        u.validate((err) => {
            expect(err.errors.firstname).to.exist;
            done();
        });
    });

    it('should be invalid if lastname is not required', (done) => {
        let u = new User();
        
        u.validate((err) => {
            expect(err.errors.lastname).to.exist;
            done();
        });
    });

    it('should be invalid if age is not required', (done) => {
        let u = new User();

        u.validate((err) => {
            expect(err.errors.age).to.exist;
            done();
        });
    });
});