const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const User = new Schema({
    lastname: {
        type    : String,
        trim    : true,
        required: [true, 'Le nom est obligatoire']
    },
    firstname: {
        type: String,
        trim: true,
        required: [true, 'Le prenom est obligatoire']
    },
    age: {
        type: Number,
        trim: true,
        required: [true, 'L\'age est obligatoire']
    }
}, {
    timestamps: true // ajoute 2 champs au document createdAt et updatedAt
});

module.exports = mongoose.model('User', User);