const User = require('../models/user.model');
const Faker = require('faker/locale/fr');

exports.getAll = async (req, res, next) => {
    try {
        let user = await User.find();

        if (user) {
            return res.status(200).json(user);
        }

        return res.status(404).json('user_not_found');
    } catch (error) {
        return res.status(501).json(error);
    }
}

exports.findThreeLasted = async (req, res, next) => {
    try {
        let user = await User.find({}).sort({date: -1}).limit(3);

        if (user) {
            return res.status(200).json(user);
        }

        return res.status(404).json('user_not_found');
    } catch (error) {
        return res.status(501).json(error);
    }
}

exports.faker = async (req, res, next) => {
    const temp = {
        firstname: Faker.name.firstName(),
        lastname: Faker.name.lastName(),
        age: Faker.datatype.number(50)
    };
    try {
        let user = await User.create(temp);
        return res.status(201).json(user);
    } catch (error) {
        return res.status(501).json(error);
    }
}

