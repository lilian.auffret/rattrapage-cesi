var express = require('express');
var router = express.Router();

const service = require('../services/user.service');

router.get('/', service.getAll);
router.post('/', service.faker);
router.get('/threelasted', service.findThreeLasted);

module.exports = router;
