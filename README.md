* Faire une API en GET qui récupère en database une liste de personne (Nom, prénom, Age). Techno de votre choix (Php, NodeJS etc.)
* Juste du JSON (que vous interrogerez avec un outils genre ARC, Postman etc.)
* 3 environnements :
  * Dev en local sur votre poste le code se run avec un conteneur Docker
  * Recette sur un serveur Linux, dans un conteneur Docker
  * Prod sur le même serveur Linux, dans un autre conteneur Docker
  * Pour chaque environnement il faut
    * Un conteneur pour run le code
    * Un conteneur pour héberger la database (une instance database pour chaque environnement)
---
Coté GIT je veux 3 branches minimum :
  * Dev
  * Stage
  * Main
    * Selon la branche que l’on push (ou merge request) ça déclenche le pipeline Devops associé
    * Chaque exécution du pipeline exécute du test unitaire (si le test ne passe pas alors le pipeline s’arrête, démontrer cette partie)
---
Pour la démo il faudra ajouter une requête API qui retourne les 3 dernières personnes enregistrées en database.
  * Vous faites votre dev ça fonctionne en dev, mais ça n’est pas déployée en Recette, ni en Prod (apporter la preuve)
  * Vous mergez le code dev sur la branche de Stage et ça met à jour l’environnement de recette
  * Vous mergez la recette sur la prod et ça met à jour l’environnement de prod
---

Vous pouvez utiliser GitLab avec le CI/CD Runner, ou bien Jenkins, ou bien Azure Devops.

* Je vous suggère sur votre serveur linux de faire des script .sh :
  * un script « Recette.sh » qui pull le code git de la branche stage et qui run les dockers (docker-compose.yml)
  * un script « Prod.sh » qui fera la meme chose mais pour la branche « main »
  * Tout ça c’est juste à titre d’exemple.
--- 
**Mettez aussi sur votre serveur Linux un « HaProxy », il s’agit d’un petit programme qui en fonction de l’URL que vous interrogez (genre http://stage.devops.local ou http://main.devops.local) va « attaquer » le bon conteneur Docker (recette / prod)**

---

# Installation

Afin de pouvoir utiliser l'application, il faut crée un dossier .docker dans lequel il faut crée un dossier nginx afin d'y mettre la configuration dans un default.conf.